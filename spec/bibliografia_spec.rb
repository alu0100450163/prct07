require "spec_helper"

=begin
describe Bibliografia do
	before :each do
    	@libro = Bibliografia.new(["Dave Thomas"],"Programming Ruby 1.9 & 2.0: The Programatic Programers Guide","4 edition","Progmatic Bookshelf","July 7, 2013",["ISBN-13: 978-1937785499","ISBN-10: 1937785491"])
		@libro.m_serie = "Informática"
  	end

	it "Comprobar que hay por lo menos un autor" do
		@libro.m_autores.length.should_not be 0
	end

	it "Debe Existir un Título." do
		expect(@libro.m_titulo).to eq("Programming Ruby 1.9 & 2.0: The Programatic Programers Guide")
	end

	it "Debe existir o no una serie" do
		expect(@libro.m_serie).to eq("Informática")
	end

	it "Debe existir una editorial." do
		expect(@libro.m_editorial).to eq("Progmatic Bookshelf")
	end

	it "Debe existir un número de edición." do
		@libro.m_num_edicion != ""
	end

	it "Debe existir una fecha de publicación." do
		@libro.m_fechaPublicacion != ""
	end

	it " Debe existir uno o más números ISBN." do
	@libro.m_isbn.length.should_not be 0
	end

	it "Existe un método para obtener el listado de autores." do
		expect(@libro.m_autores).to eq(["Dave Thomas"])
	end

	it "Existe un método para obtener el título." do
		expect(@libro.m_titulo).to eq("Programming Ruby 1.9 & 2.0: The Programatic Programers Guide")
	end

	it "Existe un método para obtener la editorial." do
		expect(@libro.m_editorial).to eq("Progmatic Bookshelf")
	end

	it "Existe un método para obtener el número de edición" do
		expect(@libro.m_num_edicion).to eq("4 edition")
	end

	it "Existe un método para obtener la fecha de publicación." do
		expect(@libro.m_fechaPublicacion).to eq("July 7, 2013")
	end

	it "Existe un método para obtener el listado de ISBN" do
		expect(@libro.m_isbn).to eq(["ISBN-13: 978-1937785499","ISBN-10: 1937785491"])
	end

	it " Existe un método para obtener la referencia formateada." do
		expect(@libro.to_s).to eq("[\"Dave Thomas\"] Programming Ruby 1.9 & 2.0: The Programatic Programers Guide 4 edition Informática Progmatic Bookshelf July 7, 2013 [\"ISBN-13: 978-1937785499\", \"ISBN-10: 1937785491\"]")

	end
end
=end
describe Node do
    before:all do
        @libro = Biblio.new(["Dave Thomas"],"Programming Ruby 1.9 & 2.0: The Programatic Programers Guide","4 edition","Progmatic Bookshelf","July 7, 2013",["888af","añfa882"])
        @node = Node.new(@libro, nil)
    end
	  it "#Debe existir un Nodo de la lista con sus datos y su siguiente" do
	     expect(@node.value).not_to eq(nil)
	  end
end

describe Lista do
	before:each do
	    @libro = Biblio.new(["Dave Thomas"],"Programming Ruby 1.9 & 2.0: The Programatic Programers Guide","4 edition","Progmatic Bookshelf","July 7, 2013",["888af","añfa882"])
		@node = Node.new(@libro,nil)
		@lista = Lista.new()
		@lista.insert(@node)
	end
	
	it "#Existe una lista vacia" do
		expect(Lista.new().empty?).to eq(true)
	end
	
	it "#Se extrae el primer elemento de la lista" do
		@lista.extractFirstElement
	end
	
	it "#Se puede insertar un elemento" do
		t_libro = Biblio.new(["Manuel"],"Como programar c++","1 edition","ULL","November 13, 2015",["ISBN-00: 288-3638585856"])
		@lista.insert(t_libro)
	end
	
	it "#Se pueden insertar varios elementos" do
		t_libro1 = Biblio.new(["Eliana"],"Aprendizaje al Procesamiento de Lenguaje Natural","3 edition","ULL-INFORMATICA","May 13, 2015",["ISBN-00: 001-1938865478"])
		t_libro2 = Biblio.new(["Orlandy"],"Introducción a las Matematicas Discretas","1 edition","ULL-MATEMATICAS","September 21, 2015",["ISBN-00: 288-25698785456"])
		@lista.insert(t_libro1)
		@lista.insert(t_libro2)
	end
	
	it "#Debe existir una Lista con su cabeza"do
	    expect(@lista.m_ini.value).to eq(@node)
	end
end